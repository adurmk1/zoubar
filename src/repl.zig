const std = @import("std");
const io = std.io;

const Lexer = @import("Lexer.zig");

const PROMPT = "[zoubar]$ ";

pub fn start(in: std.fs.File, out: std.fs.File) !void {
    const reader = in.reader();
    const writer = out.writer();
    var in_buf = io.bufferedReader(reader);
    var out_buf = io.bufferedWriter(writer);
    var buffered_reader = in_buf.reader();
    var buffered_writer = out_buf.writer();

    try buffered_writer.print("Welcome to Zoubar, a magical interpreter written in Zig\n", .{});
    try buffered_writer.print("=======================================================\n", .{});
    try buffered_writer.print("{s}", .{PROMPT});
    try out_buf.flush();

    // Size of buf determines max chars that can be read in a line
    var input_buf: [512]u8 = undefined;

    while (try buffered_reader.readUntilDelimiterOrEof(&input_buf, '\n')) |input| {
        var lexer = Lexer{};
        lexer.init(input);

        while (lexer.nextToken()) |token| {
            try buffered_writer.print("{}, {s}\n", .{ token.token_type, token.literal });
        }
        try buffered_writer.print("{s}", .{PROMPT});
        try out_buf.flush();
    }
}
