const std = @import("std");

const repl = @import("repl.zig");

pub fn main() !void {
    const stdin = std.io.getStdIn();
    const stdout = std.io.getStdOut();
    repl.start(stdin, stdout) catch |err| {
        if (err == error.StreamTooLong)
            try stdout.writer().print("ERROR: Input too long\n", .{})
        else
            return err;
    };
}
