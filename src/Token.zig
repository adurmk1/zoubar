const std = @import("std");
const mem = std.mem;

token_type: TokenType,
literal: []const u8,

pub const TokenType = enum {
    ILLEGAL,

    // Indentifiers + literals
    IDENT,
    INT,

    // Operators
    ASSIGN,
    PLUS,
    MINUS,
    BANG,
    ASTERISK,
    SLASH,
    LT,
    GT,
    EQ,
    NEQ,

    // Delimiters
    COMMA,
    SEMICOLON,

    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    LBRACKET,
    RBRACKET,

    // Keywords
    VAR,
    FUNCTION,
    RETURN,
    TRUE,
    FALSE,
    IF,
    ELSE,
    AND,
    OR,
};

// TODO Maybe use a hash algorithm
pub fn lookupIdentifier(identifier: []const u8) TokenType {
    return if (mem.eql(u8, identifier, "var")) .VAR
    else if (mem.eql(u8, identifier, "fn")) .FUNCTION
    else if (mem.eql(u8, identifier, "return")) .RETURN
    else if (mem.eql(u8, identifier, "true")) .TRUE
    else if (mem.eql(u8, identifier, "false")) .FALSE
    else if (mem.eql(u8, identifier, "if")) .IF
    else if (mem.eql(u8, identifier, "else")) .ELSE
    else if (mem.eql(u8, identifier, "and")) .AND
    else if (mem.eql(u8, identifier, "or")) .OR
    else .IDENT;
}
