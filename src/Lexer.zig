const std = @import("std");
const mem = std.mem;
const ascii = std.ascii;

const expect = std.testing.expect;

const Token = @import("Token.zig");
const TokenType = Token.TokenType;
const lookupIdentifier = Token.lookupIdentifier;

const Self = @This();

input: []const u8 = undefined,
pos: usize = 0,
char: u8 = 0,

pub fn init(self: *Self, input: []const u8) void {
    self.input = input;
    self.char = if (self.input.len > 0) self.input[0] else 0;
}

// TODO Add +=, -=, *=, /=, <=, >=
// TODO Parse two character tokens in dedicated function
pub fn nextToken(self: *Self) ?Token {
    self.skipWhitespace();
    defer self.readChar();
    return switch (self.char) {
        '=' => blk: {
            if (self.peekChar() == '=') {
                self.readChar();
                break :blk Token{ .token_type = .EQ, .literal = "==" };
            }
            break :blk Token{ .token_type = .ASSIGN, .literal = "=" };
        },
        '+' => Token{ .token_type = .PLUS, .literal = "+" },
        '-' => Token{ .token_type = .MINUS, .literal = "-" },
        '*' => Token{ .token_type = .ASTERISK, .literal = "*" },
        '/' => Token{ .token_type = .SLASH, .literal = "/" },
        '<' => Token{ .token_type = .LT, .literal = "<" },
        '>' => Token{ .token_type = .GT, .literal = ">" },
        '!' => blk: {
            if (self.peekChar() == '=') {
                self.readChar();
                break :blk Token{ .token_type = .NEQ, .literal = "!=" };
            }
            break :blk Token{ .token_type = .BANG, .literal = "!" };
        },
        '(' => Token{ .token_type = .LPAREN, .literal = "(" },
        ')' => Token{ .token_type = .RPAREN, .literal = ")" },
        '{' => Token{ .token_type = .LBRACE, .literal = "{" },
        '}' => Token{ .token_type = .RBRACE, .literal = "}" },
        ',' => Token{ .token_type = .COMMA, .literal = "," },
        ';' => Token{ .token_type = .SEMICOLON, .literal = ";" },
        0 => null,
        else => if (ascii.isAlphabetic(self.char)) blk: {
            const identifier = self.readIdentifier();
            break :blk Token{ .token_type = lookupIdentifier(identifier), .literal = identifier };
        } else if (ascii.isDigit(self.char)) blk: {
            const number = self.readNumber();
            break :blk Token{ .token_type = .INT, .literal = number };
        } else Token{ .token_type = .ILLEGAL, .literal = self.input[self.pos .. self.pos + 1] },
    };
}

fn readChar(self: *Self) void {
    self.pos += 1;
    if (self.pos >= self.input.len) self.char = 0 else self.char = self.input[self.pos];
}

fn peekChar(self: Self) u8 {
    if (self.pos + 1 >= self.input.len) return 0;
    return self.input[self.pos + 1];
}

fn readIdentifier(self: *Self) []const u8 {
    const pos = self.pos;
    while (ascii.isAlphabetic(self.char) or self.char == '_') : (self.readChar()) {}
    defer self.unreadChar(); // Function reads a char too far
    return self.input[pos..self.pos];
}

fn readNumber(self: *Self) []const u8 {
    const pos = self.pos;
    while (ascii.isDigit(self.char) or self.char == '_') : (self.readChar()) {}
    defer self.unreadChar(); // Function reads a char too far
    return self.input[pos..self.pos];
}

inline fn skipWhitespace(self: *Self) void {
    while (ascii.isWhitespace(self.char)) : (self.readChar()) {}
}

inline fn unreadChar(self: *Self) void {
    self.pos -= 1;
}

// TESTS
const ExpectedToken = struct {
    expected_type: TokenType,
    expected_literal: []const u8,
};

test "operators" {
    const input: []const u8 =
        \\ + - * / = ! < > == !=
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .PLUS, .expected_literal = "+" },
        .{ .expected_type = .MINUS, .expected_literal = "-" },
        .{ .expected_type = .ASTERISK, .expected_literal = "*" },
        .{ .expected_type = .SLASH, .expected_literal = "/" },
        .{ .expected_type = .ASSIGN, .expected_literal = "=" },
        .{ .expected_type = .BANG, .expected_literal = "!" },
        .{ .expected_type = .LT, .expected_literal = "<" },
        .{ .expected_type = .GT, .expected_literal = ">" },
        .{ .expected_type = .EQ, .expected_literal = "==" },
        .{ .expected_type = .NEQ, .expected_literal = "!=" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "delimiters" {
    const input: []const u8 =
        \\ (  ) { } , ;
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
        .{ .expected_type = .LBRACE, .expected_literal = "{" },
        .{ .expected_type = .RBRACE, .expected_literal = "}" },
        .{ .expected_type = .COMMA, .expected_literal = "," },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "keywords" {
    const input: []const u8 =
        \\ var
        \\ fn
        \\ return
        \\ true
        \\ false
        \\ if
        \\ else
        \\ and
        \\ or
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .VAR, .expected_literal = "var" },
        .{ .expected_type = .FUNCTION, .expected_literal = "fn" },
        .{ .expected_type = .RETURN, .expected_literal = "return" },
        .{ .expected_type = .TRUE, .expected_literal = "true" },
        .{ .expected_type = .FALSE, .expected_literal = "false" },
        .{ .expected_type = .IF, .expected_literal = "if" },
        .{ .expected_type = .ELSE, .expected_literal = "else" },
        .{ .expected_type = .AND, .expected_literal = "and" },
        .{ .expected_type = .OR, .expected_literal = "or" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "variables" {
    const input: []const u8 =
        \\ var five = 1;
        \\ var ten = 10;
        \\ five = 5;
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .VAR, .expected_literal = "var" },
        .{ .expected_type = .IDENT, .expected_literal = "five" },
        .{ .expected_type = .ASSIGN, .expected_literal = "=" },
        .{ .expected_type = .INT, .expected_literal = "1" },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
        .{ .expected_type = .VAR, .expected_literal = "var" },
        .{ .expected_type = .IDENT, .expected_literal = "ten" },
        .{ .expected_type = .ASSIGN, .expected_literal = "=" },
        .{ .expected_type = .INT, .expected_literal = "10" },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
        .{ .expected_type = .IDENT, .expected_literal = "five" },
        .{ .expected_type = .ASSIGN, .expected_literal = "=" },
        .{ .expected_type = .INT, .expected_literal = "5" },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "functions" {
    const input: []const u8 =
        \\ var add = fn(x, y) {
        \\     return x + y;
        \\ };
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .VAR, .expected_literal = "var" },
        .{ .expected_type = .IDENT, .expected_literal = "add" },
        .{ .expected_type = .ASSIGN, .expected_literal = "=" },
        .{ .expected_type = .FUNCTION, .expected_literal = "fn" },
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .IDENT, .expected_literal = "x" },
        .{ .expected_type = .COMMA, .expected_literal = "," },
        .{ .expected_type = .IDENT, .expected_literal = "y" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
        .{ .expected_type = .LBRACE, .expected_literal = "{" },
        .{ .expected_type = .RETURN, .expected_literal = "return" },
        .{ .expected_type = .IDENT, .expected_literal = "x" },
        .{ .expected_type = .PLUS, .expected_literal = "+" },
        .{ .expected_type = .IDENT, .expected_literal = "y" },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
        .{ .expected_type = .RBRACE, .expected_literal = "}" },
        .{ .expected_type = .SEMICOLON, .expected_literal = ";" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "function calls" {
    const input: []const u8 =
        \\ add(5, 10)
    ;
    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .IDENT, .expected_literal = "add" },
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .INT, .expected_literal = "5" },
        .{ .expected_type = .COMMA, .expected_literal = "," },
        .{ .expected_type = .INT, .expected_literal = "10" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "control flow" {
    const input: []const u8 =
        \\ if (10 > 5){
        \\ } else {
        \\ }
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .IF, .expected_literal = "if" },
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .INT, .expected_literal = "10" },
        .{ .expected_type = .GT, .expected_literal = ">" },
        .{ .expected_type = .INT, .expected_literal = "5" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
        .{ .expected_type = .LBRACE, .expected_literal = "{" },
        .{ .expected_type = .RBRACE, .expected_literal = "}" },
        .{ .expected_type = .ELSE, .expected_literal = "else" },
        .{ .expected_type = .LBRACE, .expected_literal = "{" },
        .{ .expected_type = .RBRACE, .expected_literal = "}" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "boolean expressions" {
    const input =
        \\ !(10 > 5) == false
        \\ true or false and true
        \\ 5 < 10 != true
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .BANG, .expected_literal = "!" },
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .INT, .expected_literal = "10" },
        .{ .expected_type = .GT, .expected_literal = ">" },
        .{ .expected_type = .INT, .expected_literal = "5" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
        .{ .expected_type = .EQ, .expected_literal = "==" },
        .{ .expected_type = .FALSE, .expected_literal = "false" },
        .{ .expected_type = .TRUE, .expected_literal = "true" },
        .{ .expected_type = .OR, .expected_literal = "or" },
        .{ .expected_type = .FALSE, .expected_literal = "false" },
        .{ .expected_type = .AND, .expected_literal = "and" },
        .{ .expected_type = .TRUE, .expected_literal = "true" },
        .{ .expected_type = .INT, .expected_literal = "5" },
        .{ .expected_type = .LT, .expected_literal = "<" },
        .{ .expected_type = .INT, .expected_literal = "10" },
        .{ .expected_type = .NEQ, .expected_literal = "!=" },
        .{ .expected_type = .TRUE, .expected_literal = "true" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}

test "arithmetic expressions" {
    const input: []const u8 =
        \\ 3 * (3 / 3) + 3 - 3
    ;

    const expected_tokens = [_]ExpectedToken{
        .{ .expected_type = .INT, .expected_literal = "3" },
        .{ .expected_type = .ASTERISK, .expected_literal = "*" },
        .{ .expected_type = .LPAREN, .expected_literal = "(" },
        .{ .expected_type = .INT, .expected_literal = "3" },
        .{ .expected_type = .SLASH, .expected_literal = "/" },
        .{ .expected_type = .INT, .expected_literal = "3" },
        .{ .expected_type = .RPAREN, .expected_literal = ")" },
        .{ .expected_type = .PLUS, .expected_literal = "+" },
        .{ .expected_type = .INT, .expected_literal = "3" },
        .{ .expected_type = .MINUS, .expected_literal = "-" },
        .{ .expected_type = .INT, .expected_literal = "3" },
    };

    var lexer = Self{};
    lexer.init(input);

    var i: usize = 0;
    while (lexer.nextToken()) |token| : (i += 1) {
        if (i == expected_tokens.len) try expect(false); // Lexer returns more tokens than it should
        try expect(token.token_type == expected_tokens[i].expected_type);
        try expect(mem.eql(u8, token.literal, expected_tokens[i].expected_literal));
    }
    try expect(i == expected_tokens.len);
}
